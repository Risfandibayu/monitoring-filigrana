<?php 
    use Illuminate\Support\Facades\DB;
    use Carbon\Carbon;
    ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Dashboard Template">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="stacks">
    <link rel="shortcut icon" type="image/png" href="http://localhost/microgold/assets/images/logoIcon/favicon.png">
    <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>MicroGold - Monitoring Page</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700,800&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"
        integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />




    <!-- Theme Styles -->
    <link href="{{ asset('public') }}/css/main.min.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/dark-theme.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/custom.css" rel="stylesheet">
    <link href="{{ asset('public') }}/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <style>
        .text-x {
            font-size: 10vw
        }

        .text-y {
            font-size: 3vw
        }

        .text-z {
            font-size: 2vw
        }


        .alert {
            border-radius: 0px !important;
        }

        .card {
            border-radius: 0px 0px 15px 15px !important;
        }

        .border {
            border-style: solid;
            color: #7888fc;
        }
    </style>
</head>

<body>

    <div class="page-container">
        <div class="row">
            <div class="col-6">
                <div class="card text-center" >
                    <div class="row">
                        <div class="col-8 text-danger text-z">
                            TOTAL DELIVERY PENDING
                        </div>
                        <?php 
                                    $today = db::table('sendgolds')->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count() + db::table('brodevs')->where(['ship_method'=>2])->where('status',2)->whereMonth('created_at',Carbon::now()->month)->orwhere('ship_method',null)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count();
                                    $yesterday =  db::table('sendgolds')->whereMonth('created_at',Carbon::now()->month - 1)->count() + db::table('brodevs')->where(['ship_method'=>2])->whereMonth('created_at',Carbon::now()->month - 1)->orwhere('ship_method',null)->whereMonth('created_at',Carbon::now()->month - 1)->count();
                                    // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                    if ($today == 0 || $yesterday == 0) {
                                        # code...
                                        $p = 0;
                                    }else{
                                        $p1 = ((($yesterday-$today)/$yesterday)*100);
                                        if ($p1>100) {
                                            # code...
                                            $p = $p1 - 100;
                                        }else{
                                            $p = $p1 * -1;
                                        }
                                    }
                                    ?>
                        <div class="col-4 text-y" id="tdpp">
                            <i class="fa fa-arrow-up hijau" aria-hidden="true" id="up"
                                style="display: {{$p > 0 ? '':'none'}}"></i>
                            <i class="fa fa-arrow-down text-danger" aria-hidden="true" id="down"
                                style="display: {{$p < 0 ? '':'none'}}"></i>
                            <span id="tbjp" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                                {{abs(round($p,1))}}
                            </span>
                            <span id="per" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                                %
                            </span>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <h1 class="text-x text-danger" id="tdp">
                            {{db::table('sendgolds')->where('status',2)->count() +
                            db::table('brodevs')->where(['ship_method'=>2])->where('status',2)->orwhere('ship_method',null)->where('status',2)->count();}}
                        </h1>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card text-center" >
                    <div class="row">
                        <div class="col-8 text-danger text-z">
                            DELV RETAIL PENDING ({{strtoupper(Carbon::now()->format('F'))}})
                        </div>
                        <?php 
                                            $today = db::table('sendgolds')->whereMonth('created_at',Carbon::now()->month)->where('status',2)->count();
                                            $yesterday =  db::table('sendgolds')->whereMonth('created_at',Carbon::now()->month - 1)->count();
                                            // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                            if ($today == 0 || $yesterday == 0) {
                                                # code...
                                                $p = 0;
                                            }else{
                                                $p1 = ((($yesterday-$today)/$yesterday)*100);
                                                if ($p1>100) {
                                                    # code...
                                                    $p = $p1 - 100;
                                                }else{
                                                    $p = $p1 * -1;
                                                }
                                            }
                                            ?>
                        <div class="col-4 text-y" id="dpp">
                            <i class="fa fa-arrow-up hijau" aria-hidden="true" id="up"
                                style="display: {{$p > 0 ? '':'none'}}"></i>
                            <i class="fa fa-arrow-down text-danger" aria-hidden="true" id="down"
                                style="display: {{$p < 0 ? '':'none'}}"></i>
                            <span id="tbjp" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                                {{abs(round($p,1))}}
                            </span>
                            <span id="per" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                                %
                            </span>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <h1 class="text-x text-danger" id="dp">
                            {{db::table('sendgolds')->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count()}}
                        </h1>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card text-center" >
                    <div class="row">
                        <div class="col-8 text-danger text-z">
                            PACK DELV PENDING ({{strtoupper(Carbon::now()->format('F'))}})
                        </div>
                        <?php 
                                            $today = db::table('brodevs')->where('ship_method',2)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->orwhere('ship_method',null)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count();
                                            $yesterday =  db::table('brodevs')->where('ship_method',2)->whereMonth('created_at',Carbon::now()->month-1)->orwhere('ship_method',null)->whereMonth('created_at',Carbon::now()->month-1)->count();
                                            // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                            if ($today == 0 || $yesterday == 0) {
                                                # code...
                                                $p = 0;
                                            }else{
                                                $p1 = ((($yesterday-$today)/$yesterday)*100);
                                                if ($p1>100) {
                                                    # code...
                                                    $p = $p1 - 100;
                                                }else{
                                                    $p = $p1 * -1;
                                                }
                                            }
                                            ?>
                        <div class="col-4 text-y" id="bpdpp">
                            <i class="fa fa-arrow-up hijau" aria-hidden="true" id="up"
                                style="display: {{$p > 0 ? '':'none'}}"></i>
                            <i class="fa fa-arrow-down text-danger" aria-hidden="true" id="down"
                                style="display: {{$p < 0 ? '':'none'}}"></i>
                            <span id="tbjp" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                                {{abs(round($p,1))}}
                            </span>
                            <span id="per" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                                %
                            </span>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <h1 class="text-x text-danger" id="bpdp">
                            {{db::table('brodevs')->where('ship_method',2)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->orwhere('ship_method',null)->where('status',2)->whereMonth('created_at',Carbon::now()->month)->count()}}
                        </h1>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card text-center" >
                    <div class="row">
                        <div class="col-8 text-danger text-y">
                            PICK UP PENDING
                        </div>
                        <?php 
                                            $today = db::table('brodevs')->where('ship_method',1)->where('status',2)->whereDate('created_at',Carbon::today())->count();
                                            $yesterday =  db::table('brodevs')->where('ship_method',1)->whereDate('created_at',Carbon::yesterday())->count();
                                            // $bro = db::table('users')->where('plan_id','!=',0)->whereDate('created_at','!=', Carbon::today())->count();
                                            if ($today == 0 && $yesterday == 0) {
                                                # code...
                                                $p = 0;
                                            }else if ($today != 0 && $yesterday == 0) {
                                                # code...
                                                $p = $today;
                                            }else{
                                                $p1 = ((($yesterday-$today)/$yesterday)*100);
                                                if ($p1>100) {
                                                    # code...
                                                    $p = $p1 - 100;
                                                }else{
                                                    $p = $p1 * -1;
                                                }
                                            } 
                                            ?>
                        <div class="col-4 text-y" id="bppup">
                            <i class="fa fa-arrow-up hijau" aria-hidden="true" id="up"
                                style="display: {{$p > 0 ? '':'none'}}"></i>
                            <i class="fa fa-arrow-down text-danger" aria-hidden="true" id="down"
                                style="display: {{$p < 0 ? '':'none'}}"></i>
                            <span id="tbjp" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                                {{abs(round($p,1))}}
                            </span>
                            <span id="per" class="{{$p > 0 ? 'hijau':'text-danger'}}">
                                %
                            </span>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <h1 class="text-x text-danger" id="bppu">
                            {{db::table('brodevs')->where('ship_method',1)->where('status',2)->count()}}
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    {{-- <script src="{{ asset('public') }}/plugins/jquery/jquery-3.4.1.min.js"></script> --}}
    <script>
        function tdp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.tdp') }}"
        })
        .done(function( data ) {
        $('#tdp').html(data);

        setTimeout(tdp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        tdp();
    </script>
    <script>
        function tdpp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.tdpp') }}"
        })
        .done(function( data ) {
        $('#tdpp').html(data);

        setTimeout(tdpp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        tdpp();
    </script>


    <script>
        function dp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.dp') }}"
        })
        .done(function( data ) {
        $('#dp').html(data);

        setTimeout(dp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        dp();
    </script>
    <script>
        function dpp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.dpp') }}"
        })
        .done(function( data ) {
        $('#dpp').html(data);

        setTimeout(dpp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        dpp();
    </script>

    <script>
        function bpdp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.bpdp') }}"
        })
        .done(function( data ) {
        $('#bpdp').html(data);

        setTimeout(bpdp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        bpdp();
    </script>
    <script>
        function bpdpp() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.bpdpp') }}"
        })
        .done(function( data ) {
        $('#bpdpp').html(data);

        setTimeout(bpdpp, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        bpdpp();
    </script>

    <script>
        function bppu() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.bppu') }}"
        })
        .done(function( data ) {
        $('#bppu').html(data);

        setTimeout(bppu, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        bppu();
    </script>
    <script>
        function bppup() {

        $.ajax({
        type: "GET",
        url: "{{ route('count.bppup') }}"
        })
        .done(function( data ) {
        $('#bppup').html(data);

        setTimeout(bppup, 1000);
        }).fail(function(data){
            location.reload();
        });
        }
        bppup();
    </script>

</body>

</html>